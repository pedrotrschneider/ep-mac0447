import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.camera import Camera
from android.permissions import request_permissions, Permission
from android import mActivity

import time
import os

kivy.require("2.2.1")


class CameraClick(Widget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.image_counter = 0
        self.timestr = time.strftime("%Y%m%d_%H%M%S")
        self.classes = [
            "flipflop",
            "glasses",
            "scissor",
            "knife",
            "shoe",
        ]


    def capture(self):
        camera = self.ids['camera']
        label1 = self.ids['label1']
        label2 = self.ids['label2']

        self.image_counter += 1
        
        if self.image_counter < 90:
            class_id = self.image_counter // 18
            class_name = self.classes[class_id]
            class_object_id = (self.image_counter // 6) % 3
            background_id = (self.image_counter // 3) % 2
            current_img_id = self.image_counter % 3
            background_type = 'light' if background_id == 0 else 'dark'

            context = mActivity.getApplicationContext()
            result =  context.getExternalFilesDir(None)   # don't forget the argument
            if result:
                storage_path =  str(result.toString())
            else:
                storage_path = "/sdcard"
            
            if not os.path.exists(storage_path):
                os.mkdir(storage_path)
            
            save_path = f"{storage_path}/{class_name}_object{class_object_id}_bg{background_type}_{current_img_id}.jpeg"
            print("captured")
            print(save_path)

            label1.text = f"Take a picture of {class_name} {class_object_id + 1} with a {background_type} background"
            label2.text = f"{3 - current_img_id} pictures to go"
        else: 
            save_path = ""
            label1.text = "Database fully collected!"    
            label2.text = ""


        camera.export_to_png(save_path)


class DatasetCollector(App):
    def build(self):
        request_permissions([
            Permission.CAMERA,
            Permission.WRITE_EXTERNAL_STORAGE,
            Permission.READ_EXTERNAL_STORAGE
        ])
        return CameraClick()


DatasetCollector().run()
