# EP MAC0447 - Análise e Reconhecimento de Formas: Teoria e Prática (2023)

Este repositório contém o código do meu EP da matéria de MAC0447 - Análise e Reconhecimento de Formas: Teoria e Prática (2023) do Instituto de Matemática e Estatística (IME) da Universidade de São Paulo (USP).

## Parte 1

Esta parte do EP consiste na criação de um aplicativo mobile para realizar a aquisição do dataset que será utilizado nas próximas partes do EP.

Para isso, foi utilizada a framework [Kivy](https://kivy.org/) com a linguagem de programação [Python](https://www.python.org/), e a ferramenta [Buildozer](https://buildozer.readthedocs.io/en/latest/) para gerar o arquivo `.apk` para rodar em android.

### Configuração

Para rodar o aplicativo, é necessário obter algumas dependências, mas todas elas podem ser instaladas utilizando o [pip](https://pypi.org/project/pip/).

Começamos instalando `Kivy`, a framework gráfica utilizada para desenvolver o aplicativo:

```bash
pip3 install kivy
```

Para gerar um executável de android, precisamos instalar a ferramenta `buildozer`:

```bash
pip3 install --user --upgrade buildozer
```

Além do pacote instalado com `pip`, dependências adicionais serão necessárias. Caso esteja utilizando Linux Debian ou algum derivativo, OSX, ou Windows com [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/install), [a documentação oficial](https://buildozer.readthedocs.io/en/latest/installation.html) do `buildozer` oferece instruções de instalação completas. Caso esteja utilizando alguma outra distribuição de Linux, consulte um manual de sua distro para realizar a instalação das dependências. A ferramenta não é compatível com Windows, por isso o WSL é necessário. 

### Compialção

Tendo instalado corretamente o `buildozer`, podemos gerar o arquivo `.apk` para uso em aparelhos android. Para isso, basta estar na pasta `part1` deste repositório e rodar o comando:

```bash
buildozer -v android debug
```

Esse processo irá demorar alguns minutos da primeira vez, mas execuções subsequentes irão demorar apenas alguns segundos.

Após terminado, duas novas pastas irão surgir:  
- `.buildozer` com vários arquivos de configuração do `.buildozer`
- `bin` com o arquivo `.apk` gerado pela ferramenta

Esse arquivo `.apk` pode ser transferido para qualquer dispositivo android e utilizado normalmente.

Caso tenha ativado [opções de desenvolvedor](https://en-gb.support.motorola.com/app/answers/detail/a_id/159678/~/developer-options) em seu dispositivo android, e tenha ativado [depuração por USB](https://www.embarcadero.com/starthere/xe5/mobdevsetup/android/en/enabling_usb_debugging_on_an_android_device.html), é possível conectar o dispositivo android diretamente no computador e utilizar o comando:

```bash
buildozer -v android debug deploy run
```

Ele irá automaticamente instalar e abrir o aplicativo em seu dispositivo android quando a compilação terminar.

### Utilização

A interface do aplicativo consiste no viewport da câmera centralizado na tela, o botão para tirar uma foto no centro inferior, e um texto com instruções no canto superior.

![Captura de tela do aplicativo da parte 1 funcionando](media/part1-screenshot.png)

Ao capturar uma imagem, o texto de auxílio irá atualizar as instruções para a próxima imagem. Assim que todas as imagens forem capturadas com sucesso, o texto fará essa indicação e não será mais possível capturar imagens.

![Captura de tela do aplicativo da parte 1 mostrando a mensagem de que o dataset foi coletado com sucesso](media/part1-completed.png)

### Dataset

O data set gerado pela aplicação e devidamente organizado com seus metadados também está disponível nesse repositório n oarquivo `dataset.zip`.

#### Informações sobre o dataset

|        Descrição      |     Valor     |
|:---------------------:|:-------------:|
| Numero de classes     | 5             |
| Tamanho da base       | 66 MB         |
| Resolução das imagens | 1048 x 1400   |
|                       |               |
| Classe chinelo        | 18 imagens    |
| Classe óculos         | 18 imagens    |
| Classe tesoura        | 18 imagens    |
| Classe faca           | 18 imagens    |
| Classe tênis          | 18 imagens    |
